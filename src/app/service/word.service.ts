import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TCategory } from '../models/category.model';
import { TWord } from '../models/word.model';
import { TResponse } from '../models/global.model';
import { LocalStorageService } from './local-storage.service';
import { CommonService } from './common.service';

@Injectable({
  providedIn: 'root',
})
export class WordService {
  constructor(private http: HttpClient, private localStorageService: LocalStorageService, private commonService: CommonService) {}

  private headers = new HttpHeaders().set('Authorization', '3fef9c7409b2f7a45b805096dd7517ae');

  getListWord(categoryId: number, categoryName: string): Observable<TResponse> {
    return this.http.get<TResponse>(`https://api.mazii.net/api/get-note/${categoryId}/0/100`, { headers: this.headers });
  }

  saveCategory(categoryName: string, listWord: TWord[]) {
    const newListWord = listWord.map((item) => {
      if (item.type === 'word' && this.commonService.isVietnamese(item.name)) {
        return { ...item, name: item.mean, mean: item.name };
      }
      if (item.type === 'grammarDetail') {
        const splitWord = item.name.split(': ');
        return { ...item, name: splitWord[0], mean: splitWord[1] };
      }
      return item;
    });
    // save local storage nếu category chưa có data
    if (!localStorage.getItem(categoryName)) {
      this.localStorageService.setItem(categoryName, newListWord);
    }
    return newListWord;
  }
}
