import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TCategory } from '../models/category.model';
import { TWord } from '../models/word.model';
import { MyObject, TResponse } from '../models/global.model';

@Injectable({
  providedIn: 'root',
})
export class CommonService {
  vietnameseRegex = /[\u00C0-\u1EF9\u1EFA-\u1F00\u1F08-\u1FFF]+/g;

  constructor() {}

  isVietnamese(str: string) {
    return this.vietnameseRegex.test(str);
  }

  splitStringToObj(str: string) {
    let obj: MyObject = {};
    let arr: string[] = [];
    let idxThreeDos: number = 0;
    if (str) {
      arr = str.split('').filter((item) => item.trim() !== '');
      console.log(str);
      console.log(arr);
      arr = [
        ...arr.map((item, index) => {
          if (item === '.') {
            idxThreeDos = index;
            return '...';
          }
          return item;
        }),
      ];
      if (idxThreeDos) {
        arr.splice(idxThreeDos - 2, 2);
      }
      for (let i = 0; i < arr.length; i++) {
        obj[i + 1] = arr[i];
      }
      return obj;
    }
    return obj;
  }

  getRandomArray(arr: any[], n: number) {
    var result = new Array(n),
      len = arr.length,
      taken = new Array(len);

    if (n > len) throw new RangeError('Không đủ phần tử trong mảng');

    while (n--) {
      var x = Math.floor(Math.random() * len); // Sinh số ngẫu nhiên từ 0 đến len
      result[n] = arr[x in taken ? taken[x] : x]; // Chọn phần tử từ mảng ban đầu
      taken[x] = --len in taken ? taken[len] : len; // Đánh dấu phần tử đã chọn
    }

    return result;
  }
}
