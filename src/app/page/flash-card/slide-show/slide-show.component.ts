import { Component, OnDestroy, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { TWord } from 'src/app/models/word.model';

@Component({
  selector: 'app-slide-show',
  templateUrl: './slide-show.component.html',
  styleUrls: ['./slide-show.component.scss'],
})
export class SlideShowComponent implements OnChanges {
  isShowMean = false;
  @Input() idxCurrentWord: number = 1;
  @Input() listWordLength: number = 0;
  @Input() activeWord: TWord | undefined;
  @Output() handleClickNext = new EventEmitter<void>();
  @Output() handleClickPrev = new EventEmitter<void>();

  ngOnChanges(changes: SimpleChanges): void {
    this.isShowMean = false;
  }

  setIsShowMean = (event: Event) => {
    event.stopPropagation();
    this.isShowMean = !this.isShowMean;
  };

  onClickNext = (event: Event) => {
    event.stopPropagation();
    this.handleClickNext.emit();
  };

  onClickPrev = (event: Event) => {
    event.stopPropagation();
    this.handleClickPrev.emit();
  };
}
