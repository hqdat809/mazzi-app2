import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { EFilterWord, TWord } from 'src/app/models/word.model';
import { LocalStorageService } from 'src/app/service/local-storage.service';
import { AppState } from 'src/app/store/reducers';

@Component({
  selector: 'app-flash-card',
  templateUrl: './flash-card.component.html',
  styleUrls: ['./flash-card.component.scss'],
})
export class FlashCardComponent {
  listWord: TWord[] = [];
  listWordFiltered: TWord[] = [];
  activeWordIdx: number = 0;
  filterState = EFilterWord.ALL;
  activeWord: TWord | undefined;
  categoryId: number | undefined;
  categoryName: string | undefined;
  subscription: Subscription[] = [];

  constructor(private store: Store<AppState>, private route: ActivatedRoute, private localStorageService: LocalStorageService) {}

  ngOnInit() {
    const routeSubscription = this.route.params.subscribe(({ categoryId, categoryName }) => {
      this.listWord = this.localStorageService.getItem(categoryName);
      this.listWordFiltered = this.localStorageService.getItem(categoryName);
      this.activeWord = this.listWordFiltered[0];
      this.categoryName = categoryName;
      this.categoryId = categoryId;
    });
    this.subscription.push(routeSubscription);
  }

  onClickNext = () => {
    if (this.activeWordIdx < this.listWordFiltered.length - 1) {
      this.activeWordIdx += 1;
      this.activeWord = this.listWordFiltered[this.activeWordIdx];
    } else {
      this.activeWordIdx = 0;
      this.activeWord = this.listWordFiltered[this.activeWordIdx];
    }
  };

  onClickPrev = () => {
    if (this.activeWordIdx > 0) {
      this.activeWord = this.listWordFiltered[this.activeWordIdx + -1];
      this.activeWordIdx -= 1;
    } else {
      this.activeWordIdx = this.listWordFiltered.length - 1;
      this.activeWord = this.listWordFiltered[this.activeWordIdx];
    }
  };

  shuffleArray(array: any[]): any[] {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
  }

  onClickShuffleBtn() {
    this.listWordFiltered = this.shuffleArray(this.listWordFiltered);
    this.activeWordIdx = 0;
    this.activeWord = this.listWordFiltered[this.activeWordIdx];
  }

  onFilter = (value: any) => {
    this.filterState = value.target.value;
    switch (value.target.value) {
      case EFilterWord.REMEMBER: {
        if (this.categoryName) {
          this.listWordFiltered = this.localStorageService.getItem(this.categoryName).filter((word: TWord) => word.remember);
        }
        break;
      }
      case EFilterWord.NOT_REMEMBER: {
        if (this.categoryName) {
          this.listWordFiltered = this.localStorageService.getItem(this.categoryName).filter((word: TWord) => !word.remember);
        }
        break;
      }
      case EFilterWord.ALL: {
        if (this.categoryName) {
          this.listWordFiltered = this.localStorageService.getItem(this.categoryName);
        }
        break;
      }
      default:
        return;
    }
    this.activeWord = this.listWordFiltered[0];
    this.activeWordIdx = 0;
  };

  setRememberState = (wordId: number | undefined, isRemember: boolean) => {
    if (this.categoryName) {
      switch (this.filterState) {
        case EFilterWord.ALL: {
          this.listWord = this.listWord.map((word: TWord) => {
            if (word.id === wordId) {
              return { ...word, remember: isRemember };
            }
            return word;
          });
          this.localStorageService.setItem(this.categoryName, this.listWord);

          this.listWordFiltered = this.listWord;
          this.activeWord = this.listWord.find((word: TWord) => word.id === wordId);
          break;
        }
        case EFilterWord.REMEMBER: {
          this.listWord = this.listWord.map((word: TWord) => {
            if (word.id === wordId) {
              return { ...word, remember: isRemember };
            }
            return word;
          });
          this.localStorageService.setItem(this.categoryName, this.listWord);

          this.listWordFiltered = this.listWordFiltered.map((word: TWord) => {
            if (word.id === wordId) {
              return { ...word, remember: isRemember };
            }
            return word;
          });
          this.activeWord = this.listWord.find((word: TWord) => word.id === wordId);
          break;
        }
        case EFilterWord.NOT_REMEMBER: {
          this.listWord = this.listWord.map((word: TWord) => {
            if (word.id === wordId) {
              return { ...word, remember: isRemember };
            }
            return word;
          });
          this.localStorageService.setItem(this.categoryName, this.listWord);

          this.listWordFiltered = this.listWordFiltered.map((word: TWord) => {
            if (word.id === wordId) {
              return { ...word, remember: isRemember };
            }
            return word;
          });
          this.activeWord = this.listWord.find((word: TWord) => word.id === wordId);
          break;
        }
      }

      // const newListWord = JSON.parse(
      //   localStorage.getItem(this.categoryName) || '[]'
      // ).map((word: TWord) => {
      //   if (word.id === wordId) {
      //     return { ...word, remember: isRemember };
      //   }
      //   return word;
      // });
      // localStorage.setItem(this.categoryName, JSON.stringify(newListWord));
      // this.activeWord = newListWord.find((word: TWord) => word.id === wordId);

      // check filterState to update listWord in 2 case remember and not remember
      // switch (this.filterState) {
      //   case EFilterWord.REMEMBER: {
      //     this.listWord = newListWord.filter((word: TWord) => word.remember);
      //     if (this.activeWordIdx >= this.listWord.length) {
      //       this.activeWordIdx -= 1;
      //       this.activeWord = this.listWord[this.activeWordIdx];
      //     } else {
      //       this.activeWord = this.listWord[this.activeWordIdx];
      //     }
      //     break;
      //   }
      //   case EFilterWord.NOT_REMEMBER: {
      //     this.listWord = newListWord.filter((word: TWord) => !word.remember);
      //     if (this.activeWordIdx >= this.listWord.length) {
      //       this.activeWordIdx -= 1;
      //       this.activeWord = this.listWord[this.activeWordIdx];
      //     } else {
      //       this.activeWord = this.listWord[this.activeWordIdx];
      //     }
      //     break;
      //   }
      //   case EFilterWord.ALL: {
      //     this.listWord = newListWord;
      //     this.activeWord = this.listWord[this.activeWordIdx];
      //     this.onClickNext();
      //     break;
      //   }
      //   default:
      //     return;
      // }
    }
  };

  ngOnDestroy() {
    this.subscription.forEach((sub) => sub.unsubscribe());
  }
}
