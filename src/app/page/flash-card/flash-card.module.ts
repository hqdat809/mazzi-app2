import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlashCardComponent } from './flash-card.component';
import { RouterModule } from '@angular/router';
import { SlideShowComponent } from './slide-show/slide-show.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';

export const flashCardRoutes = [{ path: '', component: FlashCardComponent }];

@NgModule({
  declarations: [FlashCardComponent, SlideShowComponent],
  imports: [CommonModule, FormsModule, NgbModule, RouterModule.forChild(flashCardRoutes)],
})
export class FlashCardModule {}
