import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FillComponent } from './fill.component';
import { ModalResultComponent } from './modal-result/modal-result.component';

export const homeRoutes = [{ path: '', component: FillComponent }];

@NgModule({
  declarations: [FillComponent, ModalResultComponent],
  imports: [CommonModule, RouterModule.forChild(homeRoutes)],
})
export class FillModule {}
