import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { MyObject } from 'src/app/models/global.model';
import { TWord } from 'src/app/models/word.model';
import { CommonService } from 'src/app/service/common.service';
import { LocalStorageService } from 'src/app/service/local-storage.service';
import { AppState } from 'src/app/store/reducers';

@Component({
  selector: 'app-fill',
  templateUrl: './fill.component.html',
  styleUrls: ['./fill.component.scss'],
})
export class FillComponent implements OnInit, OnDestroy {
  timer: any;

  listWord: TWord[] = [];
  currentWord: TWord = this.listWord[0];

  currentWordSplitArr: string[] = [];
  listChart: string[] = [];

  filledChart: MyObject = {};
  currentWordSplitObj: MyObject = {};

  arrNotFillKeys: number[] = [];
  currentWordIdx: number = 0;
  numberCorrect: number = 0;

  currentChartNeedFill: string = '';

  subscription: Subscription[] = [];
  @ViewChild('resultModal') resultModal!: ElementRef<HTMLElement>;

  constructor(private store: Store<AppState>, private route: ActivatedRoute, private localStorageService: LocalStorageService, private modalService: NgbModal, private commonService: CommonService) {}

  ngOnInit(): void {
    const routeSubscription = this.route.params.subscribe(({ categoryName }) => {
      // shuffle list word
      console.log('listWord: ', this.listWord);
      if (categoryName) {
        const listWordStorage = this.localStorageService.getItem(categoryName);
        this.listWord = this.shuffleArray(listWordStorage);
        this.setupCurrentWord();
        this.countDown(30);
      }
    });

    console.log(this.currentWordSplitArr);

    this.subscription.push(routeSubscription);
  }

  ngOnDestroy(): void {
    this.subscription.forEach((sub) => sub.unsubscribe());
    clearTimeout(this.timer);
  }

  setupCurrentWord() {
    this.currentWord = this.listWord?.[this.currentWordIdx];
    // split current word to obj and arr
    this.currentWordSplitObj = this.commonService.splitStringToObj(this.currentWord?.name);
    this.currentWordSplitArr = Object.values(this.currentWordSplitObj);
    // get list chart from list word
    this.listChart = this.shuffleArray(this.handleGetChartToFill(this.listWord, this.currentWordIdx));
    //
    this.filledChart = this.randomFillChart(this.currentWordSplitObj);
    this.arrNotFillKeys = Object.keys(this.filledChart).map((item) => Number(item));

    this.currentChartNeedFill = this.currentWordSplitObj[this.arrNotFillKeys[0]];
    console.log(this.currentChartNeedFill);
  }

  onSelect(strSelected: string) {
    if (strSelected === this.currentChartNeedFill) {
      this.filledChart[this.arrNotFillKeys[0]] = strSelected;
      this.arrNotFillKeys.shift();
      this.currentChartNeedFill = this.currentWordSplitObj[this.arrNotFillKeys[0]];
      console.log(this.currentChartNeedFill);
      // do when chose correct word
      if (!this.arrNotFillKeys.length) {
        // when fill done
        clearTimeout(this.timer);
        this.resetProgress();
        this.countDown(30);
        this.currentWordIdx += 1;
        this.numberCorrect += 1;
        if (this.currentWordIdx < this.listWord.length) {
          setTimeout(() => {
            this.setupCurrentWord();
          }, 1000);
        } else {
          // open modal log result
          setTimeout(() => {
            this.openModal();
          }, 1000);
        }
      }
    }
  }

  randomFillChart(object1: MyObject) {
    var object2: MyObject = {};
    var keys = Object.keys(object1);
    var numberChartFilled = Math.floor(keys.length / 3);
    var randomFilledKeys = this.commonService.getRandomArray(keys, numberChartFilled);

    // Sao chép key và giá trị từ Object1 sang Object2
    keys.forEach((key: any) => {
      if (randomFilledKeys.includes(key)) {
        object2[Number(key)] = object1[Number(key)];
      } else {
        object2[Number(key)] = '';
      }
    });

    return object2;
  }

  handleGetChartToFill(listWord: TWord[], currentIdx: number) {
    let arrChart: string[] = [];
    let count = 0;
    listWord?.forEach((item: TWord, index: number) => {
      if (item.id !== currentIdx && count < 3) {
        arrChart = [...arrChart, ...item.name.split('')];
        count += 1;
      }
    });
    arrChart = [...arrChart, ...listWord[currentIdx].name.split('')];
    arrChart = [
      ...arrChart.map((item) => {
        if (item === '.') {
          return '...';
        }
        return item;
      }),
    ];
    return [...new Set(arrChart)].filter((item) => item.trim() !== '');
  }

  openModal() {
    const modalRef = this.modalService.open(this.resultModal, {
      centered: true,
    });

    modalRef.result
      .then((result) => {})
      .catch((reason) => {
        console.log('closemodal');
        this.onClickReload();
      });
  }

  closeModal() {
    const modalRef = this.modalService.dismissAll();
  }

  onClickContinue() {
    clearTimeout(this.timer);
    this.closeModal();
    this.countDown(30);
    this.resetProgress();
    this.setupCurrentWord();
  }

  onClickReload() {
    clearTimeout(this.timer);
    this.closeModal();
    this.countDown(30);
    this.resetProgress();
    this.currentWordIdx = 0;
    this.numberCorrect = 0;
    this.setupCurrentWord();
  }

  resetProgress() {
    document.querySelector('.progress-inside')?.classList.remove('show');
    setTimeout(() => {
      document.querySelector('.progress-inside')?.classList.add('show');
    }, 500);
  }

  countDown(time: number) {
    this.timer = setTimeout(() => {
      this.openModal();
    }, time * 1000);
  }

  shuffleArray(array: any[]): any[] {
    if (array?.length) {
      for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
      }
    }
    return array;
  }
}
