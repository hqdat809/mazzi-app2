import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-modal-result',
  templateUrl: './modal-result.component.html',
  styleUrls: ['./modal-result.component.scss'],
})
export class ModalResultComponent {
  @Input() correctWords: number = 0;
  @Input() totalLength: number = 0;
  @Input() numberWordsWrong: number = 0;
  @Output() reload = new EventEmitter<void>();
  @Output() continue = new EventEmitter<void>();

  onReload() {
    this.reload.emit();
  }

  onContinue() {
    this.continue.emit();
  }
}
