import { Component, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { TWord } from 'src/app/models/word.model';
import { AppState } from 'src/app/store/reducers';
import { TCategory } from 'src/app/models/category.model';
import * as WordSelectors from 'src/app/store/selectors/word.selector';
import * as CategorySelectors from 'src/app/store/selectors/catergory.selectors';
import * as WordActions from '../../store/actions/word.action';
import * as CategoryActions from '../../store/actions/category.action';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalStorageService } from 'src/app/service/local-storage.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit, OnDestroy {
  categories: TCategory[] = [];
  listWord: TWord[] = [];
  selectedCategoryId: number | undefined;
  selectedCategoryName: string | undefined;
  subscriptions: Subscription[] = [];
  modalTitle = 'Modal Title';
  isModalOpen = false;
  isAvailableCategory = true;

  constructor(private store: Store<AppState>, private router: Router, private route: ActivatedRoute, private localStorageService: LocalStorageService) {}

  ngOnInit(): void {
    this.categories = this.localStorageService.getItem('categories');

    // Tối ưu truy vấn
    if (!this.categories?.length) {
      this.store.dispatch(CategoryActions.loadCategories());
      console.log('get list word 2');

      const categorySubscription = this.store.select(CategorySelectors.getCategories).subscribe((data) => {
        this.categories = data;
        this.localStorageService.setItem('categories', data);
        this.selectedCategoryId = data[0]?.categoryId;
        this.selectedCategoryName = data[0]?.categoryName.replace(/\s/g, '');
        if (this.router.url === '/' && data[0]?.categoryId) {
          // Get list word first time access
          this.store.dispatch(
            WordActions.loadListWord({
              categoryId: data[0]?.categoryId,
              categoryName: data[0]?.categoryName.replace(/\s/g, ''),
            })
          );
        }
      });
      this.subscriptions.push(categorySubscription);
    }

    const listWordSubscription = this.store.select(WordSelectors.getListWord).subscribe((data) => {
      this.listWord = data;
    });
    const routerSubscription = this.route.params.subscribe(({ categoryId, categoryName }) => {
      if (categoryId && categoryName) {
        console.log('get list word 3');
        this.selectedCategoryId = categoryId;
        this.selectedCategoryName = categoryName;
        this.store.dispatch(
          WordActions.loadListWord({
            categoryId: categoryId,
            categoryName: categoryName,
          })
        );
        this.isAvailableCategory = this.categories.some((category) => {
          return category.categoryId == categoryId;
        });
      }
    });

    this.subscriptions.push(routerSubscription, listWordSubscription);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  onClickFlashCardBtn = () => {
    this.router.navigate([this.selectedCategoryName, this.selectedCategoryId, 'flash-card']);
  };

  onClickFillBtn = () => {
    console.log(this.selectedCategoryName, this.selectedCategoryId);
    this.router.navigate([this.selectedCategoryName, this.selectedCategoryId, 'fill']);
  };
}
