import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

export const appRoutes = [
  {
    path: '',
    loadChildren: () =>
      import('./page/home/home.module').then((m) => m.HomeModule),
  },

  {
    path: ':categoryName/:categoryId/flash-card',
    loadChildren: () =>
      import('./page/flash-card/flash-card.module').then(
        (m) => m.FlashCardModule
      ),
  },

  {
    path: ':categoryName/:categoryId/fill',
    loadChildren: () =>
      import('./page/fill/fill.module').then((m) => m.FillModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
