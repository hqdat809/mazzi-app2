import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';

import { WordService } from 'src/app/service/word.service';
import * as WordActions from '../actions/word.action';

@Injectable()
export class WordEffects {
  constructor(private action$: Actions, private wordService: WordService) {}

  loadListWord$ = createEffect(() =>
    this.action$.pipe(
      ofType(WordActions.loadListWord),
      mergeMap(({ categoryId, categoryName }) => {
        return this.wordService.getListWord(categoryId, categoryName).pipe(
          map((res) => {
            const newListWord = this.wordService.saveCategory(categoryName, res.data);
            return WordActions.loadListWordSuccess({ listWord: newListWord });
          }),
          catchError((error) => of(WordActions.loadListWordFail({ error })))
        );
      })
    )
  );
}
