export interface TWord {
  id: number;
  name: string;
  mean: string;
  categoryId: number;
  type: string;
  idx: string;
  phonetic: null;
  remember: boolean;
  comment: null;
}

export enum EFilterWord {
  REMEMBER = 'REMEMBER',
  NOT_REMEMBER = 'NOT_REMEMBER',
  ALL = 'ALL',
}
