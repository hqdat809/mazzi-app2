export interface TResponse {
  total: number;
  data: any;
}

export interface MyObject {
  [key: number]: string;
}
